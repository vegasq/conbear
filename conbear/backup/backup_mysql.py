from conbear.backup.backup_generic import BackupEntity

class BackupMysql(BackupEntity):
    dump_path = "/tmp/db_dump.sql"
    node = None

    def backup(self, context, nodes):
        self.node = nodes[0]
        context.execute(
            "sudo mysqldump --defaults-file=/root/.my.localhost.cnf"+\
            " --all-databases > %s" % self.dump_path,
            node=self.node)

    def download(self, context, nodes):
        context.execute("scp -r fuel@%s:%s %s/" % (
            self.node["admin_ip"],
            self.dump_path,
            context.backup_path
        ))
