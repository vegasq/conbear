from conbear.backup.backup_generic import BackupEntity

class BackupSnapshot(BackupEntity):
    def backup(self, context, nodes):
        for node in nodes:
            context.execute(
                "nodetool -h localhost -p 7199 snapshot", node=node)

    def download(self, context, nodes):
        for node in nodes:
            snapshot_name = self._extract_snapshot_name(context, node)
            with open(
                "%s/%s/snap/snap_id" % (context.backup_path, node["hostname"]
            ), "w") as fl:
                fl.write(snapshot_name)

            context.execute("scp -r fuel@%s:/var/lib/contrail_db/data/* %s/%s/snap/" % (
                node["admin_ip"],
                context.backup_path,
                node["hostname"],
            ))

    # def download2(self, context, nodes):
    #     for node in nodes:
    #         snapshot_name = self._extract_snapshot_name(context, node)
    #         snapshot_dirs = self._extract_snapshot_folders(context, node)
    #
    #         for snap_dir in snapshot_dirs:
    #             print(">> snap_dir = %s" % snap_dir)
    #             headless_snap_dir = snap_dir.replace("/var/lib/contrail_db/data/", "")
    #
    #             context.execute("mkdir -p %s/%s/snap/%s" % (
    #                 context.backup_path,
    #                 node["hostname"],
    #                 headless_snap_dir
    #             ))
    #             context.execute(
    #                 "scp -r fuel@%s:%s/%s %s/%s/snap/%s" % (
    #                     node["admin_ip"],
    #                     snap_dir,
    #                     snapshot_name,
    #                     context.backup_path,
    #                     node["hostname"],
    #                     headless_snap_dir
    #             ))

    def _extract_snapshot_name(self, context, node):
        single_snap_dir = self._extract_snapshot_folders(context, node)[0]
        out = context.execute("ls -t %s" % single_snap_dir, node=node)
        snapshot_name = out.split("\n")[0]

        return snapshot_name

    def _extract_snapshot_folders(self, context, node):
        snapshot_dirs = context.execute(
            "find /var/lib/contrail_db/data/ -name 'snapshots'", node=node)
        return snapshot_dirs.split("\n")
