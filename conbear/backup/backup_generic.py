class BackupEntity(object):
    def backup(self, context, nodes):
        raise Exception("%s do not implement backup() method" % __class__)

    def download(self, context, nodes):
        raise Exception("%s do not implement download() method" % __class__)
