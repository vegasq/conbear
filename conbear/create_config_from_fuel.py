#!/usr/bin/python
import argparse
import json
from fuelclient import objects


def get_nodes_by_role(env, role):
    nodes = env.get_all_nodes()
    nodes_to_return = []
    for n in nodes:
        if role in n.data['roles']:
            nodes_to_return.append(n)
    return nodes_to_return


def get_admin_ip_from_node(node):
    for net in node.data["network_data"]:
        if net["name"] == "fuelweb_admin":
            return net["ip"].split("/")[0]


def get_db_ip_from_node(node):
    for net in node.data["network_data"]:
        if net["name"] == "private":
            return net["ip"].split("/")[0]


def generate_config(env_id):
    env = objects.Environment(env_id)

    config = {
        "config_nodes": [],
        "mysql_nodes": [],
        "creds": {
            "user": "fuel",
            "id_rsa": "/root/.ssh/id_rsa"
        }
    }

    config_nodes = get_nodes_by_role(env, "contrail-config")
    for node in config_nodes:
        admin_ip = get_admin_ip_from_node(node)
        db_ip = get_db_ip_from_node(node)
        config["config_nodes"].append({
            "hostname": node.data["hostname"],
            "db_ip": db_ip,
            "admin_ip": admin_ip
        })

    mysql_nodes = get_nodes_by_role(env, "aic-dbng")
    for node in mysql_nodes:
        admin_ip = get_admin_ip_from_node(node)
        config["mysql_nodes"].append({
            "admin_ip": admin_ip
        })

    with open("env_description.json", "w") as fl:
        fl.write(json.dumps(config, indent=4))


def main():
    parser = argparse.ArgumentParser(description='Backup Contrail.')
    parser.add_argument("--env", required=True, help="Env ID")
    args = parser.parse_args()

    generate_config(args.env)


if __name__ == "__main__":
    main()
