#!/usr/bin/python

import os
import json
import logging
import argparse
import paramiko
import subprocess

from conbear.backup import backup_snapshot
from conbear.restore import restore_snapshot

from conbear.backup import backup_mysql
from conbear.restore import restore_mysql


class BackupRestoreToolkit(object):
    env_description = None
    def _config(self):
        """
        env_description = {
            "config_nodes": [
                {
                    "hostname": "node-1",
                    "db_ip": "2.2.2.2"
                    "admin_ip": "1.1.1.1"
                }
            ]
        }
        """
        if self.env_description:
            return self.env_description

        with open("env_description.json", "r") as fl:
            try:
                self.env_description = json.loads(fl.read())
            except IOError:
                print("Config file not found.")
                exit(1)
        return self.env_description

    def _config_nodes(self):
        return self._config()["config_nodes"]

    def _mysql_nodes(self):
        return self._config()["mysql_nodes"]

    def execute(self, command, node=None):
        logging.warning("Executing %s on %s" % (command, node))
        if node:
            ssh = paramiko.SSHClient()
            k = paramiko.RSAKey.from_private_key_file(
                self._config()["creds"]["id_rsa"])
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(
                hostname=node["admin_ip"],
                username=self._config()["creds"]["user"],
                pkey=k)
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
            return_output = ssh_stdout.read()
            err = ssh_stderr.read()
        else:
            p = subprocess.Popen(
                command.split(" "),
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            return_output, err = p.communicate()
        logging.debug("Command: %s" % command)
        logging.debug("Output: %s" % return_output)
        logging.debug("Error: %s" % err)

        return return_output


class RestoreContrail(BackupRestoreToolkit):
    def __init__(self, path, log):
        self.backup_path = path
        self.log = log

    def do(self):
        restore_snap = restore_snapshot.RestoreSnapshot()
        restore_snap.upload(self, self._config_nodes())
        restore_snap.restore(self, self._config_nodes())

        restore_sql = restore_mysql.RestoreMysql()
        restore_sql.upload(self, self._mysql_nodes())
        restore_sql.restore(self, self._mysql_nodes())


class BackupContrail(BackupRestoreToolkit):
    def __init__(self, path, log):
        # Create fodler for our backup
        self.backup_path = path
        self.log = log

        if not os.path.exists(self.backup_path):
            os.mkdir(self.backup_path)

        # Create folder for per-node backup
        for node in self._config_nodes():
            top_level_path = "%s/%s" % (self.backup_path, node["hostname"])
            self._create_local_folder(top_level_path)
            self._create_local_folder("%s/snap" % top_level_path)

    def _create_local_folder(self, path):
        if not os.path.exists(path):
            os.mkdir(path)

    def do(self):
        backup_snap = backup_snapshot.BackupSnapshot()
        backup_snap.backup(self, self._config_nodes())
        backup_snap.download(self, self._config_nodes())

        backup_sql = backup_mysql.BackupMysql()
        backup_sql.backup(self, self._mysql_nodes())
        backup_sql.download(self, self._mysql_nodes())


def main():
    parser = argparse.ArgumentParser(description='Backup Contrail.')

    parser.add_argument("action", help="backup, restore")
    parser.add_argument("--path", required=True, help="Path to backup")
    parser.add_argument("--debug", help="Print more info", action="store_true")

    args = parser.parse_args()

    logging.basicConfig()
    LOG = logging.getLogger(__name__)

    if args.debug:
        LOG.setLevel(logging.DEBUG)

    if args.action == "backup":
        bc = BackupContrail(args.path, LOG)
        bc.do()
    elif args.action == "restore":
        rc = RestoreContrail(args.path, LOG)
        rc.do()
    else:
        raise Exception("Unknown action. Use backup or restore.")


if __name__ == "__main__":
    main()
