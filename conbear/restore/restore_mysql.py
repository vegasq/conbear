from conbear.restore.restore_generic import RestoreEntity

class RestoreMysql(RestoreEntity):
    dump_path = "/tmp/db_dump.sql"
    node = None

    def upload(self, context, nodes):
        self.node = nodes[0]

        context.execute(
            "scp -r %s/db_dump.sql fuel@%s:/tmp/" % (
                context.backup_path,
                self.node["admin_ip"]))

    def restore(self, context, nodes):
        cmd = "sudo mysql "+\
              "--defaults-extra-file=/root/.my.localhost.cnf -u root"+\
              " < %s" % self.dump_path
        context.execute(cmd, node=self.node)
