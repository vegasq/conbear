class RestoreEntity(object):
    def upload(self, context, nodes):
        raise Exception("%s do not implement upload() method" % __class__)

    def restore(self, context, nodes):
        raise Exception("%s do not implement restore() method" % __class__)
