from conbear.restore.restore_generic import RestoreEntity

class RestoreSnapshot(RestoreEntity):
    def upload(self, context, nodes):
        for node in nodes:
            context.execute("rm -rf /tmp/snap/", node=node)
            context.execute(
                "scp -r %s/%s/snap/ fuel@%s:/tmp/snap/" % (
                    context.backup_path,
                    node["hostname"],
                    node["admin_ip"]))

    def restore(self, context, nodes):
        self._stop_service(context, 'supervisor-config', nodes)
        self._stop_service(context, 'supervisor-database', nodes)

        try:
            for node in nodes:
                snapshot_name = self._extract_snapshot_name(context, node=node)

                cmd = "sudo /opt/contrail/utils/cass-db-restore.sh " +\
                      "-b /var/lib/contrail_db/data -s /tmp/snap/ -n %s" % (
                        snapshot_name)

                context.execute(cmd, node=node)
        except Exception as err:
            print(err)
        finally:
            self._start_service(context, 'supervisor-database', nodes)
            self._start_service(context, 'supervisor-config', nodes)

            self._stop_service(context, 'supervisor-control', nodes)
            self._start_service(context, 'supervisor-control', nodes)

            self._stop_service(context, 'cassandra', nodes)
            self._start_service(context, 'cassandra', nodes)

    def _extract_snapshot_name(self, context, node):
        with open(
            "%s/%s/snap/snap_id" % (context.backup_path, node["hostname"]
        ), "r") as fl:
            snapshot_name = fl.read()
        return snapshot_name

        # snapshot_dirs = context.execute(
        #     "find /tmp/snap/ -name 'snapshots'", node=node)
        # path = snapshot_dirs.split("\n")[1]
        # out = context.execute("ls -t %s" % path, node=node)
        # snapshot_name = out.split("\n")[0]
        # return snapshot_name

    def _stop_service(self, context, service_name, nodes):
        for node in nodes:
            context.execute("sudo service %s stop" % service_name, node=node)

    def _start_service(self, context, service_name, nodes):
        for node in nodes:
            context.execute("sudo service %s start" % service_name, node=node)
