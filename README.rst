# CONBEAR
## CONtrail Backup And Restore

--

### Usage:

#### Create env description file.

```bash
conbear_fuel_conf_gen --env $env_id
```

#### In folder with env description file execute:

To do backup:

```bash
conbear --path $backup_name backup
```

To do restore:

```bash
conbear --path $backup_name restore
```

